package com.atriumtech.RestassuredFramework.testing;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.atriumtech.RestassuredFramework.services.BuisinessLevel;
import com.atriumtech.RestassuredFramework.services.RunTimeVariables;
import com.atriumtech.RestassuredFramework.utilities.Services;



public class TestCaseLevel {
	
	BuisinessLevel l1;


	@BeforeMethod
	public void init()
	{
		
		 l1=new BuisinessLevel();

	}
	
	@AfterMethod
	public void writeResults() {
		l1.setUp();
	}
	

	@Test(priority=1, alwaysRun=true)
	public void logInwithValidData() throws IOException
	{
		RunTimeVariables.statuCode = 200;
		System.out.println("call to login");
		l1.doLogin("login_valid");
		Assert.assertEquals(Services.statusCode,
				RunTimeVariables.statuCode);
	}
	
	@Test(priority=2)
	public void createCompany() throws IOException
	{
		RunTimeVariables.statuCode = 200;
		System.out.println("call to create company");
		l1.CreateCompany("Create Company1");
		Assert.assertEquals(Services.statusCode,
				RunTimeVariables.statuCode);
	}
	
	@Test(priority = 3)
	public void getCompanyById() {
		RunTimeVariables.statuCode = 200;
		System.out.println("call to comapny get by id");
		l1.getCompanyById();
		Assert.assertEquals(Services.statusCode,
				RunTimeVariables.statuCode);
	}

	@Test(priority = 4)
	public void createAccount() throws IOException {
		RunTimeVariables.statuCode = 200;
		System.out.println("call to create account");
		l1.createAccount("createcompadminacct");
		Assert.assertEquals(Services.statusCode, RunTimeVariables.statuCode);
	}

}
