package com.atriumtech.RestassuredFramework.utilities;

import org.testng.annotations.BeforeGroups;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;

public class TestMongo {


	@BeforeGroups
	public MongoDatabase configDatabase() {


			MongoClient mongodb = new MongoClient("localhost", 27017);
			String dbName = "LeaveApp";
		MongoDatabase db = mongodb.getDatabase("LeaveApp");

			System.out.println("Connection to MongoDB database successfully");
		// DBCollection collection;
		// MongoCollection<Document> collections =
		// db.getCollection("TestResults");
		

		/*
		 * Document doc = new Document("name", "MongoDB") .append("type",
		 * "database").append("count", 1) .append("versions",
		 * Arrays.asList("v3.2", "v3.0", "v2.6")) .append("info", new
		 * Document("x", 203).append("y", 102));
		 * 
		 * collections.insertOne(doc);
		 */

	//	MongoIterable<String> collection1 = db.listCollectionNames();

		return db;
		
}
}
