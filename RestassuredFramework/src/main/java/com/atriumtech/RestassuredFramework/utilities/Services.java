package com.atriumtech.RestassuredFramework.utilities;

import static io.restassured.RestAssured.given;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

import com.atriumtech.RestassuredFramework.services.PreRequisites;
import com.atriumtech.RestassuredFramework.services.TestCasesResults;
import com.google.gson.Gson;
import com.mongodb.client.MongoDatabase;

public class Services {

	// SessionFilter sessionFilter = new SessionFilter();
	Gson gson = new Gson();
	public String keyparameter;
	public String valueparameter;
	public static String responseString;
	public static int statusCode;
	public static String status;
	public static String cookie1;
	public static String cookie;

	// public String URL1;
	TestCasesResults tm = new TestCasesResults();
	MongoDatabase db;
	TestMongo mg = new TestMongo();

	public void sendPostRequestWithoutCookie(Object createRequest, String URL) {
		System.out.println("In sendPostRequestWithoutCookie");
		Response res = given().spec(PreRequisites.requestSpec)
				.body(createRequest).when().post(URL).then()
				.spec(PreRequisites.responseSpec).extract().response();

		responseString = res.asString();
		statusCode = res.getStatusCode();
		status = res.getStatusLine();
		System.out.println(responseString);
		JsonPath js = new JsonPath(responseString);
		cookie = js.get("sessionId");
		System.out.println("session id is" + cookie);
	}

	public String sendGetRequest(String URL) {
		System.out.println("In sendGetRequest ");
		System.out.println("session id is" + cookie1);
		System.out.println("complete url fot company get by id" + URL);

		Response res = given().spec(PreRequisites.requestSpec).cookie(cookie1)
				.when().get(URL).then().spec(PreRequisites.responseSpec)
				.extract().response();

		responseString = res.asString();

		statusCode = res.getStatusCode();
		System.out.println(responseString);

		return responseString;

	}

	public String sendPostRequestWithCookie(Object createRequest, String URL) {
		System.out.println("In sendPostRequestWithCookie");
		System.out.println("session id is" + cookie);
		cookie1 = "uuid=" + cookie;

		Response res = given().spec(PreRequisites.requestSpec).cookie(cookie1)
				.body(createRequest).when().post(URL).then()
				.spec(PreRequisites.responseSpec).extract().response();

		responseString = res.asString();
		statusCode = res.getStatusCode();
		return responseString;
	}

	public void sendDeleteRequest(String URL) {
		System.out.println("In sendDeleteRequest");
		Response res = given().spec(PreRequisites.requestSpec)
				.cookie("uuid", cookie).when().delete(URL).then().extract()
				.response();

		responseString = res.asString();
		statusCode = res.getStatusCode();
		System.out.println(responseString);
	}

	public void sendUpdateRequest(Object createRequest, String URL) {
		System.out.println("In sendUpdateRequest");
		Response res = given().spec(PreRequisites.requestSpec)
				.body(createRequest).when().patch(URL).then().extract()
				.response();

		responseString = res.asString();
		statusCode = res.getStatusCode();
		System.out.println(responseString);
	}

	public String parseResponse(String responseString, String uniqueid) {

		JsonPath js = new JsonPath(responseString);
		String id = js.get(uniqueid);
		return id;
	}

	public TestCasesResults writeToMongo(String testcasename) {
		TestCasesResults storerequest = gson.fromJson(responseString,
				TestCasesResults.class);
		storerequest.setTestcasename(testcasename);
		storerequest.setStatuscode(statusCode);
		storerequest.setResponse(responseString);
		return storerequest;

	}

	public static void main(String args[]) {

	}
}
