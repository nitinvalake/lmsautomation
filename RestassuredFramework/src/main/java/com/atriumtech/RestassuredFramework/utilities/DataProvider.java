package com.atriumtech.RestassuredFramework.utilities;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class DataProvider {
	public String sampleRequest;

	public String getData(String sheetName, String testcasename)
			throws IOException {
		// String testcaseName="createcompany";
		// String sheetName="company";
		// fileInputStream argument
		ArrayList<String> a = new ArrayList<String>();

		FileInputStream fis = new FileInputStream(
				"D://LMSdata//ApiExcelData.xlsx");
		XSSFWorkbook workbook = new XSSFWorkbook(fis);

		int sheets = workbook.getNumberOfSheets();
		for (int i = 0; i < sheets; i++) {
			if (workbook.getSheetName(i).equalsIgnoreCase(sheetName)) {
				XSSFSheet sheet = workbook.getSheetAt(i);
				// Identify Testcases coloum by scanning the entire 1st row

				Iterator<Row> rows = sheet.iterator();
				// sheet is collection of
				// rows
				Row firstrow = rows.next();

				Iterator<Cell> ce = firstrow.cellIterator();// row is collection
															// of cells
				int k = 0;
				int coloumn = 0;
				while (ce.hasNext()) {
					Cell value = ce.next();

					if (value.getStringCellValue()
							.equalsIgnoreCase("TestCases")) {
						coloumn = k;
						break;

					}

					k++;
				}
				System.out.println(coloumn);
				System.out.println(sheet.getLastRowNum());
				while (rows.hasNext()) {

					Row r = rows.next();

					if (r.getCell(coloumn).getStringCellValue()
							.equalsIgnoreCase(testcasename)) {
						Iterator<Cell> cv = r.cellIterator();
						while (cv.hasNext()) {
							Cell c = cv.next();
							sampleRequest = r.getCell(1).getStringCellValue();
							System.out.println(sampleRequest.toString());
							// break;
						}
					}

				}
			}
		}
		return sampleRequest;

	}

	public static void main(String args[]) throws IOException {

	}
}