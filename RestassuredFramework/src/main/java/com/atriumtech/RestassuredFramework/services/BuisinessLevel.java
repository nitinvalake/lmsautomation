package com.atriumtech.RestassuredFramework.services;

import java.io.IOException;
import java.util.Map;

import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atriumtech.RestassuredFramework.utilities.DataProvider;
import com.atriumtech.RestassuredFramework.utilities.Services;
import com.atriumtech.RestassuredFramework.utilities.TestMongo;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class BuisinessLevel {
	Logger log = LoggerFactory.getLogger(BuisinessLevel.class);
	String apires;
	public String URL;

	DataProvider d = new DataProvider();
	Services s = new Services();
	// mongo db connection related variables

	// TestCasesResults tm = new TestCasesResults();
	TestMongo mg = new TestMongo();
	TestCasesResults mapobject;
	MongoDatabase db;
	Document document = new Document();
	ObjectMapper oMapper = new ObjectMapper();
	Map<String, Object> map;

	public void doLogin(String testcasename) throws IOException {
		log.trace("In doLogIn");
		URL = "user/login";
		String sampleRequest1 = d.getData("Login", testcasename);
		s.sendPostRequestWithoutCookie(sampleRequest1, URL);
		mapobject = s.writeToMongo(testcasename);
	}

	public void CreateCompany(String testcasename) throws IOException {
		log.trace("In create company");
		URL = "company/create";
		String sampleRequest1 = d.getData("Company", testcasename);
		apires = s.sendPostRequestWithCookie(sampleRequest1, URL);
		// Extracting company id from response
		RunTimeVariables.compid = s.parseResponse(apires, "companyId");
		System.out.println("company id is" + RunTimeVariables.compid);
		// Writing test case results to database
		mapobject = s.writeToMongo(testcasename);

	}

	public void getCompanyById() {
		log.trace("In get company by id ");
		System.out.println("company id is" + RunTimeVariables.compid);
		URL = "company/" + RunTimeVariables.compid;
		log.debug("URL for company get by id is" + URL);
		apires = s.sendGetRequest(URL);
		System.out.println("get company successful" + apires);
		mapobject = s.writeToMongo("In get company by id ");
		System.out.println("temp" + mapobject);
	}

	public void createAccount(String testcasename) throws IOException {
		log.trace("in create account");
		URL = "account/create";
		String sampleRequest1 = d.getData("Account", testcasename);
		// sampleRequest1.replace("5c90d3041f88ff01e4129ab0",
		// RunTimeVariables.compid);
		System.out.println(sampleRequest1);
		apires = s.sendPostRequestWithCookie(sampleRequest1, URL);
		RunTimeVariables.acctid = s.parseResponse(apires, "accountId");
		mapobject = s.writeToMongo(testcasename);
		System.out.println(mapobject);
	}



	/*
	 * public void logIn_withMissingUserName(String testcasename) throws
	 * IOException { // TODO Auto-generated method stub
	 * log.trace("in logIn_withMissingUserName"); URL = "user/login"; String
	 * sampleRequest1 = d.getData("Login", testcasename);
	 * s.sendPostRequestWithoutCookie(sampleRequest1, URL); mapobject =
	 * s.writeToMongo(testcasename);
	 * 
	 * }
	 * 
	 * public void logIn_withMissingPassword(String testcasename) throws
	 * IOException { // TODO Auto-generated method stub
	 * log.trace("in logIn_withMissingPassword"); URL = "user/login"; String
	 * sampleRequest1 = d.getData("Login", testcasename);
	 * s.sendPostRequestWithoutCookie(sampleRequest1, URL); mapobject =
	 * s.writeToMongo(testcasename); }
	 * 
	 * public void logIn_withIncorrectUrl(String testcasename) throws
	 * IOException { // TODO Auto-generated method stub
	 * log.trace("in logIn_withIncorrectUrl"); URL = "user/log/1"; String
	 * sampleRequest1 = d.getData("Login", testcasename);
	 * s.sendPostRequestWithoutCookie(sampleRequest1, URL); mapobject =
	 * s.writeToMongo(testcasename); }
	 * 
	 * public void logIn_withAllFieldsNull(String testcasename) throws
	 * IOException { // TODO Auto-generated method stub
	 * log.trace("in logIn_withAllFieldsNull"); URL = "user/login"; String
	 * sampleRequest1 = d.getData("Login", testcasename);
	 * s.sendPostRequestWithoutCookie(sampleRequest1, URL); mapobject =
	 * s.writeToMongo(testcasename); }
	 * 
	 * public void logIn_withEmptyRequest(String testcasename) throws
	 * IOException { // TODO Auto-generated method stub
	 * log.trace("in logIn_withEmptyRequest"); URL = "user/login"; String
	 * sampleRequest1 = d.getData("Login", testcasename);
	 * s.sendPostRequestWithoutCookie(sampleRequest1, URL); mapobject =
	 * s.writeToMongo(testcasename); }
	 * 
	 * public void logInwithinValidData(String testcasename) throws IOException
	 * { // TODO Auto-generated method stub
	 * log.trace("in logInwithinValidData"); URL = "user/login"; String
	 * sampleRequest1 = d.getData("Login", testcasename);
	 * s.sendPostRequestWithoutCookie(sampleRequest1, URL); mapobject =
	 * s.writeToMongo(testcasename);
	 * 
	 * }
	 */

	public void setUp() {
		db = mg.configDatabase();
		map = oMapper.convertValue(mapobject, Map.class);
		MongoCollection<Document> collections = db.getCollection("TestResults");
		document.putAll(map);
		collections.insertOne(document);
		System.out.println("temp");
	}

	public static void main(String args[]) {

	}

}
