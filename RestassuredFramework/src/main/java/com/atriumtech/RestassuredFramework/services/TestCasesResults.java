package com.atriumtech.RestassuredFramework.services;


public class TestCasesResults {

	public String testcasename;
	public String status;
	public String statusText;
	public int statuscode;
	public String response;

	public String getTestcasename() {
		return testcasename;
	}

	public void setTestcasename(String testcasename) {
		this.testcasename = testcasename;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public int getStatuscode() {
		return statuscode;
	}

	public void setStatuscode(int statuscode) {
		this.statuscode = statuscode;
	}

	@Override
	public String toString() {
		return "TestCasesResults [testcasename=" + testcasename + ", status="
				+ status + ", response=" + response + ", statuscode="
				+ statuscode + "]";
	}


}
