package com.atriumtech.RestassuredFramework.services;

import static org.hamcrest.Matchers.equalTo;

import java.util.HashMap;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

public class PreRequisites {
  // public static  HashMap<String, Object> hmap = new HashMap<String, Object>();

	public static RequestSpecBuilder builder;
    public static RequestSpecification requestSpec;
    public static ResponseSpecBuilder responsebuilder;
    public static ResponseSpecification responseSpec;
    public static String jsonresponse;
    
	

	static
	{
		builder=new RequestSpecBuilder();
		builder.setBaseUri("http://localhost:8081/api/");
		builder.setContentType("application/json");
		builder.addCookie("cookie");
		
		//builder.setBody(hmap);
	   
		requestSpec = builder.build ();
		
		
	responsebuilder = new ResponseSpecBuilder();
	//responsebuilder.expectStatusCode(200);
	responsebuilder.expectContentType(ContentType.JSON);
	//responsebuilder.expectBody("status", equalTo("success"));
	responseSpec = responsebuilder.build();

	
	
	}
	
	public static void main(String args[])
	{
		PreRequisites pr=new PreRequisites();
	}
}
